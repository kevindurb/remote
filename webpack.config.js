var webpack = require('webpack');

module.exports = {
  entry: [
    'webpack-dev-server/client?http://localhost:3000',
    'webpack/hot/only-dev-server',
    './main.jsx'
  ],
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ],
  module: {
    loaders: [
      {test: /\.jsx?$/, exclude: /node_modules/, loaders: ['react-hot', 'babel-loader?stage=1&optional=runtime']}
    ]
  },
  output: {
    path: __dirname + '/',
    filename: 'bundle.js'
  },
  devtool: 'inline-source-map'
};
