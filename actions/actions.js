var dispatcher = require('../dispatcher/dispatcher');
var {SOMETHING} = require('../constants/constants');
module.exports = {
  doSomething() {
    dispatcher.handleViewAction({
      actionType: SOMETHING
    });
  }
};
