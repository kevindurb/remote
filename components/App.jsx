var React = require('react');
var {CHANGE_EVENT} = require('../constants/constants');
var store = require('../stores/store');
var actions = require('../actions/actions');

function getState() {
  return {
  };
}

module.exports = React.createClass({
  getInitialState() {
    return getState();
  },
  componentDidMount() {
    store.on(CHANGE_EVENT, this.storeChanged);
  },
  componentWillUnmount() {
    store.off(CHANGE_EVENT, this.storeChanged);
  },
  storeChanged() {
    this.setState(getState());
  },
  render() {
    return (
      <main>
        hello
      </main>
    )
  }
});
