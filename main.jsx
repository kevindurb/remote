import React from 'react';
import App from './components/App.jsx';
document.addEventListener("DOMContentLoaded", event => {
  React.render(<App />, document.body);
});
