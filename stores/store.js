var {EventEmitter} = require('events');
var constants = require('../constants/constants');
var dispatcher = require('../dispatcher/dispatcher');

class Store extends EventEmitter {
}

var store = new Store();

dispatcher.register((payload) => {
  var action = payload.action;

  switch(action.actionType) {
    case CHANGE_EVENT:
      _message = payload.value;
      store.emit(constants.CHANGE_EVENT);
    default:
      store.emit(constants.CHANGE_EVENT);
  }

  return true;
});

module.exports = store;
