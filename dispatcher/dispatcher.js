var {Dispatcher} = require('flux');
var {VIEW_ACTION} = require('../constants/constants');

class AppDispatcher extends Dispatcher {
  handleViewAction(action) {
    this.dispatch({
      source: VIEW_ACTION,
      action
    });
  }
}

module.exports = new AppDispatcher();
